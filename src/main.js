import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import { CHECK_AUTH } from './store/actions.type'
import ApiService from './common/api.service'
Vue.config.productionTip = false

ApiService.init()

router.beforeEach((to, from, next) => {
  Promise.all([store.dispatch(CHECK_AUTH)])
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isAuthenticated) {
      next()
    } else {
      next({ name: 'login' })
    }
  } else {
    next()
  }
})

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
