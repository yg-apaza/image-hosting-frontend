import Vue from 'vue'
import ApiService from '@/common/api.service'

import { PICTURE_ADD, PICTURE_RESET_STATE, FETCH_PICTURE_DETAIL } from './actions.type'
import { RESET_STATE, SET_PICTURE_DETAIL } from './mutations.type'

function initialState () {
  return {
    picture: {
      name: '',
      file: null
    },
    picture_detail: {
      id: '',
      name: ''
    }
  }
}

export const state = {
  ...initialState()
}

export const actions = {
  [PICTURE_ADD] ({ state }) {
    let formData = new FormData()
    formData.append('name', state.picture.name)
    formData.append('file', state.picture.file)
    return ApiService.post('picture', formData)
  },
  [PICTURE_RESET_STATE] ({ commit }) {
    commit(RESET_STATE)
  },
  async [FETCH_PICTURE_DETAIL] (context, pictureId) {
    const { data } = await ApiService.get('/picture/' + pictureId, {})
    context.commit(SET_PICTURE_DETAIL, data.picture)
    return data.picture
  }
}

export const mutations = {
  [SET_PICTURE_DETAIL] (state, pictureDetail) {
    state.picture_detail.id = pictureDetail._id
    state.picture_detail.name = pictureDetail.name
  },
  [RESET_STATE] () {
    for (let f in state) {
      Vue.set(state, f, initialState()[f])
    }
  }
}

const getters = {
  picture (state) {
    return state.picture
  },
  picture_detail (state) {
    return state.picture_detail
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}
