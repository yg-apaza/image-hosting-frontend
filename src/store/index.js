import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth.module'
import picture from './picture.module'
import pictures from './pictures.module'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    picture,
    pictures
  }
})
