import ApiService from '../common/api.service'
import { FETCH_PICTURES } from './actions.type'
import { FETCH } from './mutations.type'

const state = {
  pictures: []
}

const getters = {
  pictures (state) {
    return state.pictures
  }
}

const actions = {
  [FETCH_PICTURES] ({ commit }) {
    ApiService.get('/picture', {})
      .then((data) => {
        commit(FETCH, data.data.pictures)
      })
      .catch(error => {
        throw new Error(error)
      })
  }
}

const mutations = {
  [FETCH] (state, pictures) {
    state.pictures = pictures
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
