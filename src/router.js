import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import AddPic from './views/AddPic.vue'
import ShowPic from './views/ShowPic.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      name: 'add-pic',
      path: '/picture/add',
      component: AddPic,
      meta: {
        requiresAuth: true
      }
    },
    {
      name: 'show-pic',
      path: '/picture/:id',
      component: ShowPic,
      meta: {
        requiresAuth: true
      }
    }
  ]
})
