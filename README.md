# Image Hosting Frontend

Notice: This source code is based on [Vue realworld example app](https://github.com/gothinkster/vue-realworld-example-app)

Image Hosting frontend web made with Vue.js and Vuetify. Deployed on Heroku: [image-hosting-frontend.herokuapp.com](https://image-hosting-frontend.herokuapp.com). Keep in mind that pictures are saved on ephemeral dynos system on Heroku, this is a just demo app and the images are not stored in a persistent way.

![Image Hosting Web Screenshot](screenshot.png)


## Setting up the development environment

1. Install the dependencies

```
npm install
```

2. Edit `API_URL` variable on the `src/common/config.js` file

3. Start the server in development mode

```
npm run serve
```
