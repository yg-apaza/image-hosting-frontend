// Small Express server to deploy SPA apps on Heroku
var express = require('express')
var history = require('connect-history-api-fallback')
var path = require('path')

var app = express()
const staticFileMiddleware = express.static(path.join(__dirname, '/dist'))

app.use(staticFileMiddleware)
app.use(history({
  disableDotRule: true,
  verbose: true
}))
app.use(staticFileMiddleware)

var port = process.env.PORT || 5000
app.listen(port)
